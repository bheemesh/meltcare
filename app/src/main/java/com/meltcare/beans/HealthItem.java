package com.meltcare.beans;

import com.meltcare.entities.RecyclerLayoutType;

import java.io.Serializable;

/**
 * Created by bheemesh on 14/1/16.
 *
 * @author bheemesh
 */
public class HealthItem implements Serializable, Entity<String> {
    private String id;
    private String key;
    private String displayName;
    private RecyclerLayoutType cardType;

    //---------- getter methods --------
    @Override
    public String getId() {
        return id;
    }

    //--------- setter methods-----
    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }


    public RecyclerLayoutType getCardType() {
        return cardType;
    }

    public void setCardType(RecyclerLayoutType cardType) {
        this.cardType = cardType;
    }
}
