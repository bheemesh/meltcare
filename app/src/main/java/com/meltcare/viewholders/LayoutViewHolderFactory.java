package com.meltcare.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.meltcare.R;
import com.meltcare.entities.RecyclerLayoutType;

import com.meltcare.listeners.OnRecyclerItemClickListener;

/**
 * Created by bheemesh on 2/1/16.
 */
public class LayoutViewHolderFactory {

    public static RecyclerView.ViewHolder getViewHolder(ViewGroup parent,
                                                        OnRecyclerItemClickListener viewOnBaseItemClickListener,
                                                        RecyclerLayoutType topicCardType) {
        switch (topicCardType) {
            case LINEAR_LIST:
                return new HealthIntelViewHolder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            case GRID_LIST:
                return new ProfileItemViewHolder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
            default:
                return new HealthIntelViewHolder(createView(parent, topicCardType),
                        viewOnBaseItemClickListener);
        }
    }

    private static View createView(ViewGroup parent, RecyclerLayoutType booksHomeCardType) {
        switch (booksHomeCardType) {
            case LINEAR_LIST:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_health_intel_linear, parent, false);
            case GRID_LIST:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_profile_grid, parent, false);
            default:
                return LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_health_intel_linear, parent, false);
        }
    }

}
