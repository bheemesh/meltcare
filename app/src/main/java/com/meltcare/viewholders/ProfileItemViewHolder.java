package com.meltcare.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.meltcare.R;
import com.meltcare.beans.Entity;
import com.meltcare.beans.HealthItem;
import com.meltcare.listeners.OnRecyclerItemClickListener;
import com.meltcare.listeners.UpdatableViewHolder;

/**
 * Created by bheemesh on 2/1/16.
 */
public class ProfileItemViewHolder extends RecyclerView.ViewHolder implements UpdatableViewHolder {
    private TextView title;
    private ImageView imageView;
    private OnRecyclerItemClickListener clickListener;
    private HealthItem topicObj;

    public ProfileItemViewHolder(View itemView, OnRecyclerItemClickListener clickListener) {
        super(itemView);
        this.clickListener = clickListener;
        title = (TextView) itemView.findViewById(R.id.tv_item_name);
        imageView = (ImageView) itemView.findViewById(R.id.iv_img);
        setOnclickListener(null);
    }

    private void setOnclickListener(final OnRecyclerItemClickListener clickListener) {
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("card item got clicked");
                clickListener.onItemClick(view, topicObj);
            }
        });
    }

    @Override
    public void updateViewHolder(Context context, Entity topic) {
        this.topicObj = (HealthItem) topic;
        title.setText(topicObj.getDisplayName());
        setOnclickListener(clickListener);
    }
}
