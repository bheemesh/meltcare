package com.meltcare.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.meltcare.R;
import com.meltcare.adapters.RecyclerListAdapter;
import com.meltcare.beans.HealthItem;
import com.meltcare.entities.RecyclerLayoutType;
import com.meltcare.listeners.OnRecyclerItemClickListener;

import java.util.ArrayList;

public class HealthIntelActivity extends AppCompatActivity implements OnRecyclerItemClickListener {

    public ArrayList<HealthItem> topicList;
    private RecyclerView listContainer;
    private RecyclerListAdapter adapter;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_intel);
        toolbar = (Toolbar) findViewById(R.id.htab_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listContainer = (RecyclerView) findViewById(R.id.rv_container);

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout)
                findViewById(R.id.htab_collapse_toolbar);
        collapsingToolbarLayout.setTitleEnabled(false);

        loadTopics();

        LinearLayoutManager linearManager = new LinearLayoutManager(this.getBaseContext());
        listContainer.setLayoutManager(linearManager);
        listContainer.setHasFixedSize(true);
        adapter = new RecyclerListAdapter(this, topicList, this);
        listContainer.setAdapter(adapter);
        setTitle("My Topics");
    }

    private void loadTopics() {
        topicList = new ArrayList<>();
        HealthItem item;
        for (int index = 0; index < 15; index++) {
            item = new HealthItem();
            item.setDisplayName("DOCTORS");
            item.setCardType(RecyclerLayoutType.LINEAR_LIST);
            topicList.add(item);
        }
    }

    @Override
    public void onItemClick(View view, HealthItem placeObj) {
        System.out.println("BHEEM : MyTopicsActivity : onItemClick");
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }
}
