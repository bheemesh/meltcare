package com.meltcare.listeners;

import android.view.View;

import com.meltcare.beans.HealthItem;

/**
 * Created by bheemesh on 2/1/16.
 */
public interface OnRecyclerItemClickListener {
    void onItemClick(View view, HealthItem placeObj);
}
