package com.meltcare.entities;

/**
 * Created by bheemesh on 2/1/16.
 */
public enum RecyclerLayoutType {
    LINEAR_LIST(200, "linear_list"),
    GRID_LIST(201, "grid_list");

    private int index;
    private String name;

    RecyclerLayoutType(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public static RecyclerLayoutType fromName(String name) {
        for (RecyclerLayoutType type : RecyclerLayoutType.values()) {
            if (type.name.equalsIgnoreCase(name)) {
                return type;
            }
        }
        return LINEAR_LIST;
    }

    public static RecyclerLayoutType fromIndex(int index) {
        for (RecyclerLayoutType type : RecyclerLayoutType.values()) {
            if (type.index == index) {
                return type;
            }
        }
        return LINEAR_LIST;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }
}
