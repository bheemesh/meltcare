package com.meltcare.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.meltcare.beans.HealthItem;
import com.meltcare.entities.RecyclerLayoutType;
import com.meltcare.viewholders.LayoutViewHolderFactory;

import java.util.ArrayList;

import com.meltcare.listeners.OnRecyclerItemClickListener;
import com.meltcare.listeners.UpdatableViewHolder;

/**
 * Created by bheemesh on 26/11/15.
 */
public class RecyclerListAdapter extends RecyclerView.Adapter {
    ArrayList<HealthItem> placeArrayList;
    Context context;
    OnRecyclerItemClickListener clickListener;

    public RecyclerListAdapter(Context context, ArrayList<HealthItem> topicCollection, OnRecyclerItemClickListener clickListener) {
        this.context = context;
        this.placeArrayList = topicCollection;
        this.clickListener = clickListener;
    }

    public void setPlaceCollection(ArrayList<HealthItem> topicCollection) {
        this.placeArrayList = topicCollection;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerLayoutType cardType = RecyclerLayoutType.fromIndex(viewType);
        return LayoutViewHolderFactory.getViewHolder(parent, clickListener, cardType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HealthItem topicObj = placeArrayList.get(position);
        UpdatableViewHolder updatableViewHolder = (UpdatableViewHolder) holder;
        updatableViewHolder.updateViewHolder(context, topicObj);
    }

    @Override
    public int getItemViewType(int position) {
        RecyclerLayoutType displayCardType = getContentItem(position).getCardType();
        if (displayCardType == null) {
            return -1;
        }
        return displayCardType.getIndex();
    }

    public HealthItem getContentItem(int position) {
        return placeArrayList.get(position);
    }

    @Override
    public int getItemCount() {
        return placeArrayList == null ? 0 : placeArrayList.size();
    }
}